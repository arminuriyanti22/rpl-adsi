<?php

class dashboard extends CI_Controller{
    public function index()
    {
        $data['pembayaran'] = $this -> model_pembayaran -> tampil_data()->result();
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('dashboard');
        $this->load->view('templates/footer');
    }
}