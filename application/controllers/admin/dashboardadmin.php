<?php
class dashboardadmin extends CI_Controller{
    public function index()
    {
        $this->load->view('templatesadmin/header');
        $this->load->view('templatesadmin/sidebar');
        $this->load->view('admin/dashboard');
        $this->load->view('templatesadmin/footer');
    }
}