<?php
class datapembayaran extends CI_Controller {
    public function index()
    {
        $data['pembayaran'] = $this->model_pembayaran->tampil_data()->result();
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('datapembayaran', $data);
        $this->load->view('templates/footer');
    }

    public function tambah_aksi()
    {
        $nama               = $this->input->post('nama');
        $jenis_pembayaran   = $this->input->post('jenis_pembayaran');
        $tanggal            = $this->input->post('tanggal');
        $jumlah             = $this->input->post('jumlah');
        $bukti_transaksi    = $_FILES['bukti_transaksi'];
        if ($bukti_transaksi=''){}else{
            $config ['upload_path'] = './assets/img';
            $config ['allowed_types'] = 'jpg|jpeg|png';

            $this->load->library('upload', $config);
            if(!$this->upload->do_upload('bukti_transaksi')){
                echo "Gambar Gagal diUpload!";
            }else {
                $bukti_transaksi=$this->upload->data('file_name');
            }
        }

        $data = array (
            'nama'              => $nama,
            'jenis_pembayaran'  => $jenis_pembayaran,
            'tanggal'           => $tanggal,
            'jumlah'            => $jumlah,
            'bukti_transaksi'   => $bukti_transaksi
        );

        $this->model_pembayaran->tambah_data($data, 'pembayaran');
        redirect('datapembayaran/index');
    }

    public function edit_data($id_pembayaran)
    {
        $where = array('id_pembayaran' =>$id_pembayaran);
        $data['pembayaran'] = $this->model_pembayaran->edit_data($where,'pembayaran')->result();
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('edit_data', $data);
        $this->load->view('templates/footer');
    }

    public function update_data()
    {
        $id_pembayaran      = $this->input->post('id_pembayaran');
        $nama               = $this->input->post('nama');
        $jenis_pembayaran   = $this->input->post('jenis_pembayaran');
        $tanggal            = $this->input->post('tanggal');
        $jumlah             = $this->input->post('jumlah');
        $bukti_transaksi    = $_FILES['bukti_transaksi']['name'];
        if ($bukti_transaksi=''){}else{
            $config ['upload_path'] = './uploads';
            $config ['allowed_types'] = 'jpg|jpeg|png|gif';

            $this->load->library('upload', $config);
            if(!$this->upload->do_upload('bukti_transaksi')){
                echo "Gambar Gagal diUpload!";
            }else {
                $bukti_transaksi=$this->upload->data('file_name');
            }
        }

        $data = array (
            'nama'              => $nama,
            'jenis_pembayaran'  => $jenis_pembayaran,
            'tanggal'           => $tanggal,
            'jumlah'            => $jumlah,
            'bukti_transaksi'   => $bukti_transaksi
        );
        $where = array(
            'id_pembayaran'     => $id_pembayaran
        );

        $this->model_pembayaran->update_data($where,$data,'pembayaran'); 
        redirect('datapembayaran/index');
    }

    public function hapus_data($id_pembayaran)
    {
        $where = array('id_pembayaran' => $id_pembayaran);
        $this->model_pembayaran->hapus_data($where, 'pembayaran');
        redirect('datapembayaran/index');
    }

    public function detail($id_pembayaran){
        $this->load->model('model_pembayaran');
        $detail = $this->model_pembayaran->detail_data($id_pembayaran);
        $data['detail'] = $detail;
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('detail', $data);
        $this->load->view('templates/footer');
    }
}