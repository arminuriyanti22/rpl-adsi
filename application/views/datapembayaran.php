<div class="container-fluid">
    <button class="btn btn-sm btn-primary mb-3" data-toggle="modal" data-target="#tambah_data"><i class="fas fa-plus fa-sm"></i>Tambah Data</button>
    <table class ="table table-bordered">
        <tr>
            <th>NO</th>
            <th>NAMA</th>
            <th>JENIS PEMBAYARAN</th>
            <th>TANGGAL</th>
            <th>JUMLAH</th>
            <th colspan="3">AKSI</th>
        </tr>
        <?php
        $no=1;
        foreach($pembayaran as $byr ) : ?>
        <tr>
            <td><?php echo $no++ ?></td>
            <td><?php echo $byr->nama ?></td>
            <td><?php echo $byr->jenis_pembayaran ?></td>
            <td><?php echo $byr->tanggal ?></td>
            <td><?php echo $byr->jumlah ?></td>
            <td><?php echo anchor('datapembayaran/detail/'.$byr->id_pembayaran, '<div class= "btn btn-success btn-sm"><i class="fa fa-search-plus"></i></div>')?></td>
            <td><?php echo anchor('datapembayaran/edit_data/'.$byr->id_pembayaran, '<div class="btn btn-primary btn-sm"><i class="fa fa-edit"></li></div>') ?></td>
            <td onclick="javascript: return confirm('Anda yakin ingin menghapus data ini?')">
            <?php echo anchor('datapembayaran/hapus_data/'.$byr->id_pembayaran, '<div class="btn btn-danger btn-sm"><i class="fa fa-trash"></li></div>') ?></td> 
        </tr>
        <?php endforeach; ?>

    </table>
</div>

<!-- Modal -->
<div class="modal fade" id="tambah_data" tabindex="-1"role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">TAMBAH PEMBAYARAN</h4> 
      </div>
      <div class="modal-body">
      <form action="<?php echo base_url(). 'datapembayaran/tambah_aksi';?>" method="post" enctype="multipart/form-data" >
       <div class="form-group">
           <label>Nama</label>
           <input type="text" name="nama" class="form-control">
       </div> 
       <div class="form-group">
           <label>Jenis pembayaran</label>
           <select name="jenis pembayaran" class="form-control">
              <option>Simpan</option>
              <option>Pinjam</option>
           </select>
       </div>
       <div class="form-group">
           <label>Tanggal</label>
           <input type="date" name="tanggal" class="form-control">
       </div>
       <div class="form-group">
           <label>Jumlah</label>
           <input type="text" name="jumlah" class="form-control">
       </div>
       <div class="form-group">
           <label>Bukti Transaksi</label>
           <input type="file" name="bukti_transaksi" class="form-control">
       </div>
       <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
      </div>
    </div>
  </div>
</div>