<div class="container-fluid">
    <selection class="content">
        <h4><strong>DETAIL PEMBAYARAN</strong></h4>

        <table class="table">
            <tr>
                <th>Nama</th>
                <td><?php echo $detail->nama?></td>
            </tr>
            <tr>
                <th>Jenis Pembayaran</th>
                <td><?php echo $detail->jenis_pembayaran?></td>
            </tr>
            <tr>
                <th>Tanggal</th>
                <td><?php echo $detail->tanggal?></td>
            </tr>
            <tr>
                <th>Jumlah</th>
                <td><?php echo $detail->jumlah?></td>
            </tr>
            <tr>
                <td>
                    <img src="<?php echo base_url(); ?>assets/img/<?php echo $detail->bukti_transaksi; ?>
                    " width="180" height="220">
                </td>
                <td></td>
            </tr>
        </table>
        <a href="<?php echo base_url('datapembayaran/index'); ?>" class="btn btn-primary">Kembali</a>
    </selection>
</div>
