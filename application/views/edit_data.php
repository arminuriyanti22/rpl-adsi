<div class="container-fluid">
    <section class="content">
        <?php foreach($pembayaran as $byr) : ?>
        <form action="<?php echo base_url().'pembayaran/update_data'?>"
        method="post">

        <div class="form-group">
           <label>Nama</label>
           <input type="hidden" name="id_pembayaran" class="form-control"
           value="<?php echo $byr->id_pembayaran?>">
           <input type="text" name="nama" class="form-control"
           value="<?php echo $byr->nama?>">
        </div>

        <div class="form-group">
           <label>Jenis Pembayaran</label>
           <select name="jenis pembayaran" class="form-control" value="<?php echo $byr->jenis_pembayaran?>">
              <option>Simpan</option>
              <option>Pinjam</option>
            </select>
        </div>

        <div class="form-group">
           <label>Tanggal</label>
           <input type="date" name="tanggal" class="form-control"
           value="<?php echo $byr->tanggal?>">
        </div>

        <div class="form-group">
           <label>Jumlah</label>
           <input type="text" name="jumlah" class="form-control"
           value="<?php echo $byr->jumlah?>">
        </div>

        <button type="reset" class="btn btn-danger">Reset</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
        <?php endforeach; ?>
    </section>
</div>