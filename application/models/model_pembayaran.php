<?php

class model_pembayaran extends CI_Model{
    public function tampil_data(){
        return $this->db->get('pembayaran');
    }
    public function tambah_data($data,$table)
    {
        $this->db->insert($table,$data);
    }
    
    public function edit_data($where,$table)
    {
        return $this->db->get_where($table,$where);
    }

    public function update_data($where,$data,$table)
    {
        $this->db->where($where);
        $this->db->update($table,$data);
    }

    public function hapus_data($where,$table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
    
    public function detail_data($id_pembayaran = null){
        $query = $this->db->get_where('pembayaran', array('id_pembayaran => $id_pembayaran'))->row();
        return $query;
    }
}
